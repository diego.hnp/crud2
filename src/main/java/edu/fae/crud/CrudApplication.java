package edu.fae.crud;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import edu.fae.crud.model.Capitulo;
import edu.fae.crud.model.Categoria;
import edu.fae.crud.model.Livro;
import edu.fae.crud.model.Noticia;
import edu.fae.crud.model.Tag;
import edu.fae.crud.model.Usuario;
import edu.fae.crud.repository.CategoriaRepository;
import edu.fae.crud.repository.LivroRepository;
import edu.fae.crud.repository.NoticiaRepository;
import edu.fae.crud.repository.TagRepository;
import edu.fae.crud.repository.UsuarioRepository;

@SpringBootApplication
public class CrudApplication {

	@Bean
	CommandLineRunner criaUsuarios(UsuarioRepository usuarioRepository){
		return args -> {
			Usuario usuario = new Usuario();
			usuario.setNome("Admin");
			usuario.setEmail("admin@fae.edu");
			usuario.setPassword("12345");
			usuarioRepository.save(usuario);			
		};
	}
	
	//Dados iniciais da base de dados
	@Bean
	CommandLineRunner runner(NoticiaRepository noticiaRepository, CategoriaRepository categoriaRepository, TagRepository tagRepository, LivroRepository livroRepository){
		return args -> {
			
			Categoria artigos = 
					categoriaRepository.save(new Categoria("Artigos"));
			
			Categoria esportes = 
					categoriaRepository.save(new Categoria("Esportes"));			
			
			noticiaRepository.save(new Noticia("FAE realiza o I Simpósio de Direito Penal", "Evento, aberto ao público, reuniu especialistas no Teatro Bom Jesus", artigos));
			noticiaRepository.save(new Noticia("Dia do Psicólogo na FAE", "Veja como foi a celebração dos alunos e professores da graduação", artigos));
			noticiaRepository.save(new Noticia("Sustentabilidade em foco", "FAE participou de um dos principais eventos sobre o tema no Brasil", esportes));
			noticiaRepository.save(new Noticia("Pedagogia: curso promove debates sobre carreira", "Profissionais convidadas compartilharam experiências e apontaram tendências do mercado de trabalho", esportes));
			
			tagRepository.save(new Tag("Automóveis"));
			tagRepository.save(new Tag("Culinária"));
			tagRepository.save(new Tag("Política"));
			tagRepository.save(new Tag("Saúde"));
			
			Livro l = new Livro("Livro 1", "ABC1");
			l.getCapitulos().add(new Capitulo("Cap 1"));
			l.getCapitulos().add(new Capitulo("Cap 2"));
			l.getCapitulos().add(new Capitulo("Cap 3"));
					
			livroRepository.save(l);
			livroRepository.save(new Livro("Livro 2", "ABC2"));
			livroRepository.save(new Livro("Livro 3", "ABC3"));
			
		};
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:8080");
            }
        };
    }
	
	public static void main(String[] args) {
		SpringApplication.run(CrudApplication.class, args);
	}
}
